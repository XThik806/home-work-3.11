
/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */


const btnclick = document.getElementById('btn-click');

btnclick.addEventListener('click', function() {
   const p = document.createElement('p');
   p.innerText = "New Paragraph";
   const section = document.querySelector('#content');
   section.append(p);
})
 

const btnInputCreate = document.createElement('button');
btnInputCreate.innerText="Input Create";
btnInputCreate.id = 'btn-input-create'

const section = document.querySelector('#content');
section.append(btnInputCreate)

btnInputCreate.addEventListener('click', function() {
   const input = document.createElement('input');
   input.type = 'text';
   input.placeholder = "New Input";
   input.name = 'new name';
   input.id = 'input-text'
   const section = document.querySelector('#content');
   section.after(input);
})